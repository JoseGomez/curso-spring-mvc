package com.curso_spring_mvc.sp_mvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpMvcApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpMvcApplication.class, args);
	}

}
